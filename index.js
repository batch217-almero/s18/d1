//console.log("Test connection");

//PARAMETER AND ARGUMENTS
//Sample w/out parameter
function printInput(){
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}

printInput();

// Sample with parameter and argument
// "name" is the parameter
// "Juana" is the argument
function printName(name){
	console.log("My name is " + name);
}

printName("Juan");
printName("Donny");

let sampleVariable = "Yui";
printName(sampleVariable);

function checkDivisibilityBy8(num) {
	let remainder = num%8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?")
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(32);

// FUNCTIONS AS ARGUMENTS

function argumentFunction(){
	console.log("This function was passed as an argument before the message.");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

console.log(argumentFunction);

// MULTIPLE PARAMETERS
function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName("Dhonel", "Ortile", "Almero");
createFullName("Dhonel", "Ortile");

// MULTIPLE PARAMETER USING STORED DATA IN VARIABLE
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// PARAMETER ARGUMENTS

function printFullName(middleName, firstName, lastName) {
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

printFullName("Juan", "Dela", "Cruz");


// RETURN STATEMENTS  W/OUT USING VARIABLE
function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("Test console");
}

let completeName = returnFullName("Juan", "Dela", "Cruz");
console.log(completeName);

// RETURN USING VARIABLE
function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
	console.log("This will not be displayed");
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);


// 
function printPlayerInfo(username, level, job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
}

let user1 = printPlayerInfo("Knight_white", 95, "Paladdin");
console.log = (user1);